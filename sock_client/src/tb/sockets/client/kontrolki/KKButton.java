/**
 * 
 */
package tb.sockets.client.kontrolki;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JButton;

/**
 * @author tb
 *
 */
@SuppressWarnings("serial")
public class KKButton extends JButton {

	private BufferedImage[] rysunki = new BufferedImage[3];
	private int stan = 0;
	
	public KKButton() {
		super("");

		BufferedImage emptyField = new BufferedImage(100, 100, BufferedImage.TYPE_INT_ARGB);
		emptyField.getGraphics().setColor(Color.BLACK);
		emptyField.getGraphics().drawLine(30, 50, 60, 50);
		rysunki[0] = emptyField;

		BufferedImage circleField = new BufferedImage(100, 100, BufferedImage.TYPE_INT_ARGB);
		circleField.getGraphics().setColor(Color.BLACK);
		circleField.getGraphics().drawOval(20, 20, 60, 60);
		rysunki[1] = circleField;

		BufferedImage crossField = new BufferedImage(100, 100, BufferedImage.TYPE_INT_ARGB);
		crossField.getGraphics().setColor(Color.BLACK);
		crossField.getGraphics().drawLine(0, 0, 100, 100);
		crossField.getGraphics().drawLine(0, 100, 100, 0);
		rysunki[2] = crossField;

		setType("");
	}

	public void setType(String type) {
		if(type.equals("x")) {
			stan = 2;
		}
		else if(type.equals("o")) {
			stan = 1;
		}
		else {
			stan = 0;
		}
	}

	/* (non-Javadoc)
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	@Override
	protected void paintComponent(Graphics g) {
		// TODO Auto-generated method stub
		super.paintComponent(g);
		g.drawImage(rysunki[stan], 0, 0, null);
	}
}
