package tb.sockets.client;

import tb.sockets.client.kontrolki.KKButton;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;

public class OrderPane extends JPanel implements Runnable {
	private java.util.List<KKButton> fields;
	private Client client;
	private String myType;

	/**
	 * Create the panel.
	 */
	public OrderPane() {

		setBackground(new Color(255, 255, 240));

		JPanel panel_1 = new JPanel();
		add(panel_1);
		panel_1.setBackground(Color.BLACK);
		panel_1.setMinimumSize(new Dimension(480, 440));
		panel_1.setPreferredSize(new Dimension(480, 440));
		panel_1.setLayout(new GridLayout(3, 3, 0, 0));

		fields = new ArrayList<>();
		for(int i = 0; i < 9; i++) {
			KKButton btn = new KKButton();
			final int ii = i;
			btn.addActionListener(e -> selectField(ii + 1));
			//btn.setEnabled(false);
			panel_1.add(btn);
			fields.add(btn);
		}
	}

	public void setClient(Client client){
		this.client = client;
	}

	private void selectField(int number){
		client.send(""+number);
	}

	@Override
	public void run() {
		try {
			myType = client.receive();
			System.out.println("MyType: " + myType);
			while (true){
				String msg = client.receive();
				System.out.println("MSG: " + msg);
				if (msg.startsWith("SET_FIELD")) {
					setField(msg);
				}
				else if(msg.startsWith("CHANGE_ROUND")) {
					changeRound(msg);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void changeRound(String msg) {
		String currentType = msg.substring(13);
		System.out.println("[DEBUG] CurrentType:" + currentType);

		if (myType.equals(currentType)) {
			System.out.println("Types matched (enabling)...");
			for(JButton btn : fields) {
				btn.setEnabled(true);
				btn.repaint();
			}
		}
		else {
			System.out.println("Types mismatched (disabling)...");
			for(JButton btn : fields) {
				btn.setEnabled(false);
				btn.repaint();
			}
		}
		repaint();
	}

	private void setField(String msg) {
		int fieldIndex =  Integer.valueOf(msg.substring(10, 11));
		String type = msg.substring(12, 13);

		fields.get(fieldIndex).setType(type);
		fields.get(fieldIndex).repaint();
		repaint();
	}
}
