package tb.sockets.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by Latitude on 2017-12-20.
 */
public class Client {

    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;

    public Client(String ip, int port) {
        try {
            socket = new Socket(ip, port);
            in = new BufferedReader(new InputStreamReader(
                    socket.getInputStream()));
            out = new PrintWriter(socket.getOutputStream(), true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String receive() throws IOException {
       return in.readLine();
    }

    public void send(String msg){
        out.println(msg);
    }

}
