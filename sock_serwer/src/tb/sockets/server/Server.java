package tb.sockets.server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

	private String gameField[];
	private Socket sx, so;
	private BufferedReader sxIn, soIn;
	private PrintWriter sxOut, soOut;
	private ServerSocket serverSocket;
	private Boolean isRoundOfX;

	public Server(int port) throws IOException {
		gameField = new String [9];
		serverSocket = new ServerSocket(port);
		isRoundOfX = true;

		for (int i=0; i<9; i++){
			gameField[i] = "";
		}
	}

	public void run() throws Exception{
		System.out.println("Waiting for X player...");
		sx = serverSocket.accept();
		System.out.println("Client connected...");
		sxIn = new BufferedReader(new InputStreamReader(
				sx.getInputStream()));
		sxOut = new PrintWriter(sx.getOutputStream(), true);
		sxOut.println("x");
		System.out.println("Player X received type...");

		System.out.println("Waiting for O player...");
		so = serverSocket.accept();
		System.out.println("Client connected...");
		soIn = new BufferedReader(new InputStreamReader(
				so.getInputStream()));
		soOut = new PrintWriter(so.getOutputStream(), true);
		soOut.println("o");
		System.out.println("Player O received type...");

		while (true){
			sendRound();
			updateField(getFieldIndex());
			check();

			isRoundOfX = !isRoundOfX;
		}
	}

	private void sendToPlayers(String msg){
		sxOut.println(msg);
		soOut.println(msg);
	}

	private void sendRound(){
		if (isRoundOfX){
			sendToPlayers("CHANGE_ROUND x");
		}
		else {
			sendToPlayers("CHANGE_ROUND o");
		}
	}

	private int getFieldIndex() throws Exception{
		String msg = isRoundOfX ? sxIn.readLine() : soIn.readLine();
		return Integer.valueOf(msg);
	}

	private void updateField(int fieldIndex){
		String fieldValue = isRoundOfX ? "x" : "o";
		gameField[fieldIndex] = fieldValue;
		sendToPlayers(String.format("SET_FIELD %d %s", fieldIndex, fieldValue));
	}

	private void check(){
		if ( (!gameField[0].equals("") && gameField[0].equals(gameField[1]) && gameField[1].equals(gameField[2])) ||
			(!gameField[3].equals("") && gameField[3].equals(gameField[4]) && gameField[4].equals(gameField[5])) ||
			(!gameField[6].equals("") && gameField[6].equals(gameField[7]) && gameField[7].equals(gameField[8])) ||
			(!gameField[0].equals("") && gameField[0].equals(gameField[3]) && gameField[3].equals(gameField[6])) ||
			(!gameField[1].equals("") && gameField[1].equals(gameField[4]) && gameField[4].equals(gameField[7])) ||
			(!gameField[2].equals("") && gameField[2].equals(gameField[5]) && gameField[5].equals(gameField[8])) ||
			(!gameField[0].equals("") && gameField[0].equals(gameField[4]) && gameField[4].equals(gameField[8])) ||
			(!gameField[2].equals("") && gameField[2].equals(gameField[4]) && gameField[4].equals(gameField[6]))
		) {
			String winner = isRoundOfX ? "x" : "o";
			sendToPlayers(String.format("END_GAME %s", winner));
		}
	}

	public static void main(String[] args) {
		try {
			new Server(5555).run();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
